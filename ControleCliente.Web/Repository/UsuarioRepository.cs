﻿using ControleCliente.Web.Context;
using ControleCliente.Web.Models;
using ControleCliente.Web.ViewModel;
using System.Data.Entity;
using System.Linq;


namespace ControleCliente.Web.Repository
{

    public class UsuarioRepository
    {
        private ControlClientContext db = new ControlClientContext();


        //Salvar um Usuario no Banco de Dados.
        public bool SaveUsuario(UsuarioSignInViewModel newUser)
        {
            bool loginJaCadastrado = db.Usuarios.ToList().Exists(x => x.NomeUsuario == newUser.NomeUsuario);
            if (loginJaCadastrado)
                return false;

            Usuario user = new Usuario()
            {
                NomeUsuario = newUser.NomeUsuario,
                Senha = newUser.Senha,
                NomeCompleto = newUser.NomeCompleto,
                Email = newUser.Email,
                Telefone = newUser.Telefone
            };

            db.Usuarios.Add(user);
            db.SaveChanges();
            return true;
        }

        //Validar se Usuario está Cadastrado
        public bool CheckUsuario(UsuarioLoginViewModel user)
        {
            //usa uma consulta LINQ para percorrer a lista de usuarios no banco de dados
            //e retorna true caso ja possui um usuario com o mesmo login e senha caso contrario retorna false.
            return db.Usuarios.ToList()
                .Exists(x => x.NomeUsuario == user.Login && x.Senha == user.Senha);
        }

        //Procura no banco e retorna um UsuarioViewModel através do parametro string login.
        public UsuarioViewModel GetUsuario(string login)
        {
            Usuario user = db.Usuarios.ToList()
                .Find(x => x.NomeUsuario == login);

            UsuarioViewModel userViewModel = new UsuarioViewModel()
            {
                NomeUsuario = user.NomeUsuario,
                Senha = user.Senha,
                NomeCompleto = user.NomeCompleto,
                Email = user.Email,
                Telefone = user.Telefone
            };

            return userViewModel;
        }

        //recebe um parametro login string e retorna a viewmodel do usuario no banco que possui aquele mesmo login.
        public UsuarioSignInViewModel GetUsuarioSignIn(string login)
        {
            Usuario user = db.Usuarios.ToList()
                .Find(x => x.NomeUsuario == login);

            UsuarioSignInViewModel userViewModel = new UsuarioSignInViewModel()
            {
                UsuarioID = user.UsuarioID,
                NomeUsuario = user.NomeUsuario,
                Senha = user.Senha,
                NomeCompleto = user.NomeCompleto,
                Email = user.Email,
                Telefone = user.Telefone,
                ConfirmarSenha = user.Senha
                
            };

            return userViewModel;
        }

        //senha não é alterada.
        public bool UpdateUsuario(UsuarioSignInViewModel user)
        {
            Usuario usuario = db.Usuarios.ToList().Find(x => x.UsuarioID == user.UsuarioID);
            if (usuario == null)
                return false;

            if (db.Usuarios.ToList().Exists(x => x.NomeUsuario == user.NomeUsuario
             && x.UsuarioID != user.UsuarioID))
                return false;

            usuario.NomeUsuario = user.NomeUsuario;
            usuario.NomeCompleto = user.NomeCompleto;
            usuario.Email = user.Email;
            usuario.Telefone = user.Telefone;

            db.Usuarios.Attach(usuario);
            db.Entry(usuario).State = EntityState.Modified;
            db.SaveChanges();
            
            return true;
        }

        public bool ChangePassword(string login,string senhaAntiga,string novaSenha,string confirmarNovaSenha)
        {
            //verifica se as novas senhas sao iguais.
            if (confirmarNovaSenha != novaSenha)
                return false;

            //retorna um usuario do banco que possuia um login igual ao do usuario logado no sistema
            //e que possua a senha igual a senha a ser substituida
            Usuario usuario = db.Usuarios.ToList().Find(x => x.NomeUsuario == login && x.Senha == senhaAntiga);
            if(usuario == null)
                return false;

            usuario.Senha = novaSenha;

            db.Usuarios.Attach(usuario);
            db.Entry(usuario).State = EntityState.Modified;
            db.SaveChanges();

            return true;
        }

        public int GetUsuarioID(string login)
        {
            return db.Usuarios.ToList().Find(x => x.NomeUsuario == login).UsuarioID;
        }
    }
}