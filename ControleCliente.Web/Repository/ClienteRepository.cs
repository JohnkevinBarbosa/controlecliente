﻿using ControleCliente.Web.Context;
using ControleCliente.Web.Models;
using ControleCliente.Web.ViewModel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ControleCliente.Web.Repository
{
    public class ClienteRepository
    {
        ControlClientContext db = new ControlClientContext();

        //Retornar todos os clientes associados a um usuario.
        public IEnumerable<ClienteViewModel> GetClientes(int id)
        {
            var list = db.Clientes.ToList().Where(x => x.UsuarioID == id);
            var listViewModel = new List<ClienteViewModel>();

            foreach (var item in list)
            {
                listViewModel.Add(
                    new ClienteViewModel()
                    {
                        UsuarioID = item.UsuarioID,
                        NomeFantasia = item.NomeFantasia,
                        RazaoSocial = item.RazaoSocial,
                        CNPJ = item.CNPJ,
                        Bairro = item.Bairro,
                        Cep = item.Cep,
                        ClienteID = item.ClienteID,
                        Complemento = item.Complemento,
                        Logradouro = item.Logradouro,
                        Municio = item.Municio,
                        Numero = item.Numero
                    });
            }
            return listViewModel;
        }

        public ClienteViewModel GetCliente(int id)
        {
            var Cliente = db.Clientes.Find(id);
            if (Cliente == null)
                return null;

            ClienteViewModel clienteViewModel = new ClienteViewModel()
            {
                UsuarioID = Cliente.UsuarioID,
                NomeFantasia = Cliente.NomeFantasia,
                RazaoSocial = Cliente.RazaoSocial,
                CNPJ = Cliente.CNPJ,
                Bairro = Cliente.Bairro,
                Cep = Cliente.Cep,
                ClienteID = Cliente.ClienteID,
                Complemento = Cliente.Complemento,
                Logradouro = Cliente.Logradouro,
                Municio = Cliente.Municio,
                Numero = Cliente.Numero
            };

            return clienteViewModel;
        }

        public bool AddCliente(ClienteViewModel ClienteView)
        {
            if (db.Clientes.ToList().Exists(x => x.CNPJ == ClienteView.CNPJ))
                return false;
            Cliente cliente = new Cliente()
            {
                Bairro = ClienteView.Bairro,
                Cep = ClienteView.Cep,
                CNPJ = ClienteView.CNPJ,
                Complemento = ClienteView.Complemento,
                Logradouro = ClienteView.Logradouro,
                Municio = ClienteView.Municio,
                NomeFantasia = ClienteView.NomeFantasia,
                Numero = ClienteView.Numero,
                RazaoSocial = ClienteView.RazaoSocial,
                UsuarioID = ClienteView.UsuarioID

            };
            db.Clientes.Add(cliente);
            db.SaveChanges();
            return true;
        }

        public void RemoveCliente(int ClienteID)
        {
            Cliente cliente = db.Clientes.Find(ClienteID);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
        }

        //percorre a lista e verifica se o cnpj já esta sendo usado em outro cliente.
        public bool VerificarCNPJExistente(string cnpj,int clienteID)
        {
            return db.Clientes.ToList().Exists(x => x.CNPJ == cnpj && x.ClienteID != clienteID);
        }

        public bool UpdateCliente(ClienteViewModel clienteModel)
        {
            Cliente cliente = db.Clientes.Find(clienteModel.ClienteID);
            if (cliente == null)
                return false;
            cliente.CNPJ = clienteModel.CNPJ;
            cliente.RazaoSocial = clienteModel.RazaoSocial;
            cliente.NomeFantasia = clienteModel.NomeFantasia;
            cliente.Municio = clienteModel.Municio;
            cliente.Logradouro = clienteModel.Logradouro;
            cliente.Numero = clienteModel.Numero;
            cliente.Cep = clienteModel.Cep;
            cliente.Complemento = clienteModel.Complemento;

            db.Clientes.Attach(cliente);
            db.Entry(cliente).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }
    }
}