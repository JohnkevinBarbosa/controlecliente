﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ControleCliente.Web.Models
{
    public class Cliente
    {
        public int ClienteID { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJ { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public string Municio { get; set; }
        public string Cep { get; set; }

        public int UsuarioID { get; set; }

        [ForeignKey("UsuarioID")]
        public virtual Usuario usuario { get; set; }
    }
}