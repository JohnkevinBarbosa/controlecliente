﻿using System.Collections.Generic;

namespace ControleCliente.Web.Models
{
    public class Usuario
    {
        public int UsuarioID { get; set; }

        public string NomeUsuario { get; set; }

        public string Senha { get; set; }

        public string NomeCompleto { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}