﻿using System.Linq;
using System.Web.Mvc;
using ControleCliente.Web.ViewModel;
using ControleCliente.Web.Repository;
using System.Net;
using System.Web.Security;
using ControleCliente.Web.Helper;

namespace ControleCliente.Web.Controllers
{

    public class UsuarioController : Controller
    {
        private UsuarioRepository usuarioRepository = new UsuarioRepository();
        private ClienteRepository clienteRepository = new ClienteRepository();

        [Authorize]
        public ActionResult Detalhes()
        {
            UsuarioViewModel usuario = usuarioRepository.GetUsuario(User.Identity.Name);
            if (usuario == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(usuario);
        }

        [Authorize]
        public ActionResult Edit()
        {
            
            UsuarioSignInViewModel user = usuarioRepository.GetUsuarioSignIn(User.Identity.Name);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(user);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit([Bind(Include = "UsuarioID,NomeUsuario,Senha,ConfirmarSenha,NomeCompleto,Email,Telefone")] UsuarioSignInViewModel usuario)
        {
            UsuarioLoginViewModel login = new UsuarioLoginViewModel()
            {
                Login = usuario.NomeUsuario,
                Senha = usuario.ConfirmarSenha
            };
            if(!usuarioRepository.CheckUsuario(login))
            {
                ModelState.AddModelError("", "Senha Invalida.");
                return View(usuario);
            }
            if (usuarioRepository.UpdateUsuario(usuario))
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.SetAuthCookie(usuario.NomeUsuario, false);
                return RedirectToAction("Detalhes");
            }
            ModelState.AddModelError("", "Login já Cadastrado no Sistema.");
            return View(usuario);
        }

        [Authorize]
        [HttpPost]
        public void AlterarSenha(string OldSenha, string NewSenha, string ConfirmNewSenha)
        {
            //string OldSenha,string NewSenha,string ConfirmNewSenha
            usuarioRepository.ChangePassword(User.Identity.Name, OldSenha, NewSenha, ConfirmNewSenha);
            

        }

        [Authorize]
        public ActionResult Index()
        {
            int usuarioID = usuarioRepository.GetUsuarioID(User.Identity.Name);
            var list = clienteRepository.GetClientes(usuarioID);
            return View(list);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Index(string pesquisa)
        {
            int usuarioID = usuarioRepository.GetUsuarioID(User.Identity.Name);
            var list = clienteRepository.GetClientes(usuarioID);
            list = list.Where(x => StringSearch.StartsWith(x.RazaoSocial,pesquisa));
            int size = list.ToList().Count;
            var list2 = list.ToList();
            if (Request.IsAjaxRequest())
                return PartialView("_Clientes", list);

            return View(list);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ExcluirCliente(int id)
        {
            clienteRepository.RemoveCliente(id);
            int Id = usuarioRepository.GetUsuarioID(User.Identity.Name);
            if (Request.IsAjaxRequest())
                return PartialView("_Clientes",clienteRepository.GetClientes(Id));

            return RedirectToAction("Index");
        }
    }
}