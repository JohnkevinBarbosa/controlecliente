﻿using ControleCliente.Web.Repository;
using ControleCliente.Web.ViewModel;
using System.Web.Mvc;
using System.Web.Security;

namespace ControleCliente.Web.Controllers
{
    public class ContaController : Controller
    {
        private UsuarioRepository usuarioRepository = new UsuarioRepository();
        

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioLoginViewModel usuario)
        {
            if (usuarioRepository.CheckUsuario(usuario)) //função UsuarioRepository.CheckUsuario() verifica se o login é
            {                                            //valido no banco de dados
                FormsAuthentication.SetAuthCookie(usuario.Login, usuario.LembrarMe);
                return RedirectToAction("Index", "Usuario");
            }
            ModelState.AddModelError("", "Login e/ou Senha Invalidos.");
            return View(usuario);
        }

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(UsuarioSignInViewModel usuario)
        {
            if (usuarioRepository.SaveUsuario(usuario))
            {
                FormsAuthentication.SetAuthCookie(usuario.NomeUsuario, false);
                return RedirectToAction("Index", "Usuario");
            }

            ModelState.AddModelError("", "Login já cadastrado."); 
            return View(usuario);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Conta");
            
        }
    }
}