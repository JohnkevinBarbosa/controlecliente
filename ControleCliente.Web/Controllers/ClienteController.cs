﻿using ControleCliente.Web.Helper;
using ControleCliente.Web.Repository;
using ControleCliente.Web.ViewModel;
using System.Net;
using System.Web.Mvc;

namespace ControleCliente.Web.Controllers
{
    public class ClienteController : Controller
    {
        private ClienteRepository clienteRepository = new ClienteRepository();
        private UsuarioRepository usuarioRepository = new UsuarioRepository();
        // GET: Client
        [Authorize]
        public ActionResult Detalhes(int ClienteID)
        {
            var Cliente = clienteRepository.GetCliente(ClienteID);
            if (Cliente == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(Cliente);
        }

        [Authorize]
        public ActionResult Editar(int ClienteID)
        {
            var Cliente = clienteRepository.GetCliente(ClienteID);
            if (Cliente == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(Cliente);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Editar([Bind(Include = "ClienteID,RazaoSocial,NomeFantasia,CNPJ,Logradouro,Numero,Bairro,Complemento,Municio,Cep,UsuarioID")]ClienteViewModel cliente)
        {
            if (!MyValidator.ValidarCNPJ(cliente.CNPJ))
            {
                ModelState.AddModelError("", "CNPJ Inválido");
                return View(cliente);
            }
            if (clienteRepository.VerificarCNPJExistente(cliente.CNPJ, cliente.ClienteID))
            {
                ModelState.AddModelError("", "CNPJ Inválido");
                return View(cliente);
            }
            if (clienteRepository.UpdateCliente(cliente))
            {
                return RedirectToAction("Index", "Usuario");
            }
            ModelState.AddModelError("", "CNPJ Inválido");
            return View(cliente);
        }

        
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(ClienteViewModel cliente)
        {
            int usuarioID = usuarioRepository.GetUsuarioID(User.Identity.Name);
            cliente.UsuarioID = usuarioID;
            if(!MyValidator.ValidarCNPJ(cliente.CNPJ))
            {
                ModelState.AddModelError("", "CNPJ Inválido");
                return View(cliente);
            }
            if (clienteRepository.AddCliente(cliente))
                return RedirectToAction("Index","Usuario");

            ModelState.AddModelError("", "CNPJ já está em uso");
            return View(cliente);
        }
    }
}