﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ControleCliente.Web.ViewModel
{
    public class UsuarioViewModel
    {

        [Display(Name = "Nome de usuario")]
        public string NomeUsuario { get; set; }

        [Display(Name = "Senha")]
        public string Senha { get; set; }

        [Display(Name = "Nome Completo")]
        public string NomeCompleto { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        public string Telefone { get; set; }
    }
}