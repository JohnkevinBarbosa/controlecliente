﻿using System.ComponentModel.DataAnnotations;

namespace ControleCliente.Web.ViewModel
{
    public class ClienteViewModel
    {
        public int ClienteID { get; set; }
        [Display(Name = "Razão Social")]
        [Required]
        public string RazaoSocial { get; set; }
        [Display(Name = "Nome Fantasia")]
        public string NomeFantasia { get; set; }
        [Required]
        public string CNPJ { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public string Municio { get; set; }
        public string Cep { get; set; }
        public int UsuarioID { get; set; }
    }
}