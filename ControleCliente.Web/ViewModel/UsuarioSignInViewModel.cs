﻿using System.ComponentModel.DataAnnotations;

namespace ControleCliente.Web.ViewModel
{
    public class UsuarioSignInViewModel
    {
        public int UsuarioID { get; set; }
        [Required(ErrorMessage = "Digite seu nome de usuario.")]
        [Display(Name = "Nome de usuario")]
        [RegularExpression(@"^\w*", ErrorMessage = "o nome de usuario não deve conter character especiais.")]
        public string NomeUsuario { get; set; }

        [Required(ErrorMessage = "Digite sua senha.")]
        [Display(Name = "Senha:")]
        [RegularExpression(@"^\w*$", ErrorMessage = "Senha não deve conter character especiais.")]
        public string Senha { get; set; }

        [Compare("Senha",ErrorMessage = "Senhas não coincidem")]
        public string ConfirmarSenha { get; set; }

        [Required(ErrorMessage = "Digite seu nome completo")]
        [Display(Name = "Nome Completo")]
        [RegularExpression(@"^[A-z' ']*$", ErrorMessage = "Nome Invalido")]
        public string NomeCompleto { get; set; }

        [Display(Name = "E-mail")]
        [RegularExpression(@"^\w*(\.\w*)?@\w*\.[a-z]+(\.[a-z]+)?$", ErrorMessage = "Email Invalido")]
        public string Email { get; set; }


        public string Telefone { get; set; }
    }
}