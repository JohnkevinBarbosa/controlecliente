﻿using System.ComponentModel.DataAnnotations;

namespace ControleCliente.Web.ViewModel
{
    public class UsuarioLoginViewModel
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        [Display(Name ="Lembrar Me")]
        public bool LembrarMe { get; set; }

    }
}