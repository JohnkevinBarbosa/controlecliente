﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControleCliente.Web.Helper
{
    public class StringSearch
    {
        public static bool StartsWith(string nome,string pesquisa="")
        {
            var list = nome.Split(' ');
            
            foreach (var item in list)
            {
                var a = item.ToLower();
                var b = pesquisa.ToLower();
                bool c = item.ToLower().StartsWith(pesquisa.ToLower());
                if (c)
                {
                    return true;
                }
            }
            
            return false;
            

            //return list.ToList().Exists(x => x.ToLower().StartsWith(pesquisa.ToLower()));
        }
    }
}