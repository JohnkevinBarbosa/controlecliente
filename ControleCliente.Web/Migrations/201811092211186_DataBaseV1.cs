namespace ControleCliente.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataBaseV1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cliente", "RazaoSocial", c => c.String());
            AlterColumn("dbo.Cliente", "NomeFantasia", c => c.String());
            AlterColumn("dbo.Cliente", "CNPJ", c => c.String());
            AlterColumn("dbo.Cliente", "Municio", c => c.String());
            AlterColumn("dbo.Cliente", "Cep", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cliente", "Cep", c => c.String(nullable: false));
            AlterColumn("dbo.Cliente", "Municio", c => c.String(nullable: false));
            AlterColumn("dbo.Cliente", "CNPJ", c => c.String(nullable: false));
            AlterColumn("dbo.Cliente", "NomeFantasia", c => c.String(nullable: false));
            AlterColumn("dbo.Cliente", "RazaoSocial", c => c.String(nullable: false));
        }
    }
}
